

import UIKit
import RealmSwift

class Cars: Object {
    
    @objc dynamic var id = ""
    @objc dynamic var name = ""
    @objc dynamic var brand = ""
    @objc dynamic var model = ""
    @objc dynamic var imageUrl = ""
    @objc dynamic var price = ""
    @objc dynamic var transmission = ""
    @objc dynamic var lat = ""
    @objc dynamic var long = ""
    
    @objc dynamic var agentEmail = ""
    @objc dynamic var isNewProduct = false
    
    
    
}
