

import UIKit

class CarDetails: NSObject {
    
    static let shared = CarDetails()
    
    var carImageUrl: String = String()
    var carBrand: String = String()
    var carName: String = String()
    var carModel: String = String()
    var carTrans: String = String()
    var longStr: String = String()
    var latStr: String = String()
    var agentEmail: String = String()
    
    
    func setCarImageUrl(url: String){
        carImageUrl = url
    }
    
    func getCarImageUrl() -> String {
        return carImageUrl
    }
    
    func setCarBrand(brand: String){
        carBrand = brand
    }
    
    func getCarBrand() -> String {
        return carBrand
    }
    
    func setCarName(name: String){
        carName = name
    }
    
    func getCarName() -> String {
        return carName
    }
    
    func setCarModel(model: String){
        carModel = model
    }
    
    func getCarModel() -> String {
        return carModel
    }
    
    func setCarTrans(trans: String){
        carTrans = trans
    }
    
    func getCarTrans() -> String {
        return carTrans
    }
    
    func setLong(long: String){
        longStr = long
    }
    
    func getLong() -> String {
        return longStr
    }
    
    func setLat(lat: String){
        latStr = lat
    }
    
    func getLat() -> String {
        return latStr
    }
    
    func setAgentEmail(email: String){
        agentEmail = email
    }
    
    func getAgentEmail() -> String {
        return agentEmail
    }
    
   

}
