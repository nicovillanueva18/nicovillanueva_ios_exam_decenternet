
import UIKit
import RealmSwift

class DBClass: NSObject {
    
    static let shared = DBClass()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func addCar(id: String, name: String, brand: String, model: String, image: String, price: String, transmission: String, newProduct: Bool, long: String, lat: String, agentEmail: String){
        let carItem = Cars()
        
        carItem.id = id
        carItem.name = name
        carItem.brand = brand
        carItem.model = model
        carItem.imageUrl = image
        carItem.price = price
        carItem.transmission = transmission
        carItem.isNewProduct = newProduct
        carItem.long = long
        carItem.lat = lat
        carItem.agentEmail = agentEmail

        try! appDelegate.realm!.write({
            appDelegate.realm!.add(carItem)

        })
        
    }
    
    func deleteLocalRecords(){
        let carDB = appDelegate.realm!.objects(Cars.self)
        try! appDelegate.realm!.write {
            appDelegate.realm!.delete(carDB)
            
        }
    }
    

}
