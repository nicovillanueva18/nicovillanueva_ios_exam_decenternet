
import UIKit

class HomeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCarName: UILabel!
    @IBOutlet weak var lblCarModel: UILabel!
    @IBOutlet weak var lblCarBrand: UILabel!
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTransmission: UILabel!
    @IBOutlet weak var imgNewItem: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgCar.layer.borderWidth = 1
        imgCar.layer.masksToBounds = false
        imgCar.layer.borderColor = UIColor.white.cgColor
        imgCar.layer.cornerRadius = imgCar.frame.height/2
        imgCar.contentMode = .scaleAspectFill
        imgCar.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
