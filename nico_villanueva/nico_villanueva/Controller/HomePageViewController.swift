

import UIKit
import SwiftyJSON
import RealmSwift
import SDWebImage

class HomePageViewController: UIViewController {

    @IBOutlet weak var tblCarList: UITableView!
    
    private let refreshControl = UIRefreshControl()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var carsArr: Results<Cars> {
        get {
            return appDelegate.realm!.objects(Cars.self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializedRefresh()

        getCarData()
        
    }
    
    private func initializedRefresh(){
        
        if #available(iOS 10.0, *) {
            tblCarList.refreshControl = refreshControl
        } else {
            tblCarList.addSubview(refreshControl)
        }
        
        refreshControl.addTarget(self, action: #selector(refreshWeatherData(_:)), for: .valueChanged)
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.tintColor = UIColor.white
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing please wait", attributes: attributes)
        
        
    }
    
    @objc private func refreshWeatherData(_ sender: Any) {
        
        getCarData()
        
    }
    
    private func reloadData(){
        self.refreshControl.endRefreshing()
        self.tblCarList.reloadData()
    }
    
    private func getCarData(){
        APIServices.shared.getCarList(page: "1", maxItems: "10", completion: { (result) in
            
            APIResponse.shared.saveCarDataToLocalDB(carArr: result)
            self.reloadData()
            
            
        })
        
    }
    
    private func requestSort(sort: String){
        
        APIServices.shared.sortCars(sortBy: sort, page: "1", maxItems: "10", completion: { (result) in
            APIResponse.shared.saveCarDataToLocalDB(carArr: result)
            self.reloadData()
            })
        
    }
    
    private func pushToViewController(viewConName: String){
        self.navigationController!.pushViewController(self.storyboard!.instantiateViewController(withIdentifier: viewConName) as UIViewController, animated: true)
        
    }
    
    @IBAction func btnSort(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "Sort your result by", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Oldest", style: .default , handler:{ (UIAlertAction)in
            self.requestSort(sort: "oldest")
        }))
        
        alert.addAction(UIAlertAction(title: "Newest", style: .default , handler:{ (UIAlertAction)in
            self.requestSort(sort: "newest")
        }))
        
        alert.addAction(UIAlertAction(title: "Price-Low", style: .default , handler:{ (UIAlertAction)in
            self.requestSort(sort: "price-low")
        }))
        
        alert.addAction(UIAlertAction(title: "Price-High", style: .default, handler:{ (UIAlertAction)in
            self.requestSort(sort: "price-high")
        }))
        
        alert.addAction(UIAlertAction(title: "Mileage-Low", style: .default, handler:{ (UIAlertAction)in
            self.requestSort(sort: "mileage-low")
        }))
        
        alert.addAction(UIAlertAction(title: "Mileage-High", style: .default, handler:{ (UIAlertAction)in
            self.requestSort(sort: "mileage-high")
        }))
        
        self.present(alert, animated: true, completion: {})
        
    }
    
    @IBAction func btnInfo(_ sender: Any) {
        
        let alert = UIAlertController(title: "Submitted By", message: "Nico Aurelio L. Villanueva", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
}

extension HomePageViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarCell", for: indexPath) as! HomeTableViewCell
        
        let carItems = carsArr[indexPath.row]
        
        cell.lblCarName.text = "Name: \(carItems.name)"
        cell.lblCarBrand.text = carItems.brand
        cell.lblCarModel.text = "Model: \(carItems.model)"
        cell.lblPrice.text = " Price: \(carItems.price)"
        cell.lblTransmission.text = "Transmission: \(carItems.transmission)"
        cell.imgNewItem.isHidden = !carItems.isNewProduct
        cell.imgCar.sd_setImage(with:URL(string: carItems.imageUrl),
                                placeholderImage: UIImage(named: "placeholder.png"))
        
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let carItems = carsArr[indexPath.row]
        
        CarDetails.shared.setCarImageUrl(url: carItems.imageUrl)
        CarDetails.shared.setCarBrand(brand: carItems.brand)
        CarDetails.shared.setCarModel(model: carItems.model)
        CarDetails.shared.setCarTrans(trans: carItems.transmission)
        CarDetails.shared.setLong(long: carItems.long)
        CarDetails.shared.setLat(lat: carItems.lat)
        CarDetails.shared.setAgentEmail(email: carItems.agentEmail)
        CarDetails.shared.setCarName(name: carItems.name)
        
        self.pushToViewController(viewConName: "CarDetailsViewControllerID")
    }
    
    
    
}
