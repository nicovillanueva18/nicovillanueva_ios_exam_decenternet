
import UIKit
import MapKit
import SDWebImage
import MessageUI

class CarDetailsViewController: UIViewController {

    
    @IBOutlet weak var imgCar: UIImageView!
    
    @IBOutlet weak var lblBrand: UILabel!
    @IBOutlet weak var lblModel: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTransmission: UILabel!
    @IBOutlet weak var mvLocation: MKMapView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCarData()
        
        let span : MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(Double(CarDetails.shared.getLat())!,
                                                                          Double(CarDetails.shared.getLong())!)
        let region: MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
        
        mvLocation.setRegion(region, animated: true)
        
        //3
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = ""
        annotation.subtitle = "Store Location"
        mvLocation.addAnnotation(annotation)
        
        
        
    }
    
    private func loadCarData (){
        
        lblBrand.text = "Brand: \(CarDetails.shared.getCarBrand())"
        lblModel.text = "Model: \(CarDetails.shared.getCarModel())"
        lblName.text = CarDetails.shared.getCarName()
        lblTransmission.text = "Transmission: \(CarDetails.shared.getCarTrans())"
        
        imgCar.layer.borderWidth = 1
        imgCar.layer.masksToBounds = false
        imgCar.layer.borderColor = UIColor.white.cgColor
        imgCar.contentMode = .scaleAspectFill
        imgCar.clipsToBounds = true
        
        imgCar.sd_setImage(with:URL(string: CarDetails.shared.getCarImageUrl()),
                           placeholderImage: UIImage(named: "placeholder.png"))
        
    }
    
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnInfo(_ sender: Any) {
        
        let alert = UIAlertController(title: "Submitted By", message: "Nico Aurelio L. Villanueva", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func btnContact(_ sender: Any) {
        let composeVC = MFMailComposeViewController()
        
        let repStr = CarDetails.shared.getAgentEmail()
        composeVC.mailComposeDelegate = self
        
        composeVC.setToRecipients([repStr])
        composeVC.setSubject("Requestion for Quotation")
        composeVC.setMessageBody("", isHTML: false)
        
        self.present(composeVC, animated: true, completion: nil)
    }
    
}

extension CarDetailsViewController: MFMailComposeViewControllerDelegate{
    
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        

        controller.dismiss(animated: true, completion: nil)
    }
}

