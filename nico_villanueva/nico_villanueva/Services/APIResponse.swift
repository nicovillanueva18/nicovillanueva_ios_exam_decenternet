

import UIKit
import SwiftyJSON

class APIResponse: NSObject {
    
    static let shared = APIResponse()
    
    func saveCarDataToLocalDB(carArr: NSArray){
        
        for i in 0 ..< (carArr.count) {
            
            DBClass.shared.addCar(id: JSON(carArr)[i]["data"]["id"].string!,
                                  name: JSON(carArr)[i]["data"]["original_name"].string!,
                                  brand: JSON(carArr)[i]["data"]["brand"].string!,
                                  model: JSON(carArr)[i]["data"]["brand_model"].string!,
                                  image: JSON(carArr)[i]["images"][0]["url"].string!,
                                  price: JSON(carArr)[i]["data"]["original_price"].string!,
                                  transmission: JSON(carArr)[i]["data"]["transmission"].string!,
                                  newProduct: JSON(carArr)[i]["data"]["new-product"].bool!,
                                  long: JSON(carArr)[i]["data"]["location_longitude"].string!,
                                  lat: JSON(carArr)[i]["data"]["location_latitude"].string!,
                                  agentEmail: JSON(carArr)[i]["data"]["item_contact_email"].string!)
        }
        
    }

}
