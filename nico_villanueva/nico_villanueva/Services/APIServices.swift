

import UIKit
import Alamofire
import SwiftyJSON

class APIServices: NSObject {
    
    static let shared = APIServices()
    
    func getCarList(page: String, maxItems: String, completion: @escaping (NSArray)->Void) {

        
        Alamofire.request(API.BASE_URL + "/page:\(page)" + "/maxitems:\(maxItems)/",
                          method: .get,
                          parameters:nil,
                          encoding: JSONEncoding.default,
                          headers: nil).responseJSON {
                            response in
                            switch response.result {
                                
                            case .success(let data):
                                let json = JSON(data)
                                let jsonArr =  json["metadata"]["results"].array
                                
                                DBClass.shared.deleteLocalRecords()
                                
                                completion(jsonArr! as NSArray)
                                
                                break
                            case .failure(let error):
                                
                                print(error)
                            }
                            
        }
    }
    
    
    func sortCars(sortBy sort: String, page: String, maxItems: String, completion: @escaping (NSArray)->Void){
        
        Alamofire.request(API.BASE_URL + "/page:\(page)" + "/maxitems:\(maxItems)" + "/sort:\(sort)/",
            method: .get,
            parameters:nil,
            encoding: JSONEncoding.default,
            headers: nil).responseJSON {
                response in
                switch response.result {
                    
                case .success(let data):
                    let json = JSON(data)
                    let jsonArr =  json["metadata"]["results"].array
                    
                    DBClass.shared.deleteLocalRecords()
                    
                    completion(jsonArr! as NSArray)
                    
                    break
                case .failure(let error):
                    
                    print(error)
                }
                
        }
        
    }
    
    
}
