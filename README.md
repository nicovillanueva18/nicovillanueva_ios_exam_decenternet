# NicoVillanueva_iOS_Exam_Decenternet

iOS Exam Submitted by Nico Villanueva

Note: Please Run the Project in Device, I used email composer for "Request Quotation to Agent" And MapKit for Store Location

Library Used:

Alamofire -for API Services
SDImageLoader - For Caching image
Realm Swift - For local DB storage.
SwiftyJson - For JSON Parsing

Design Pattern: MVC

Device Supported: iPhone and iPad

Thanks You Guys! See you soon!

Nico Aurelio Villanueva
Software Engineer
email:nicoaureliovillanueva@gmail.com
Contact Number: 09454020769
